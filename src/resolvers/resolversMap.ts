import { IResolvers } from "graphql-tools";
import query from "./query";
import mutation from './mutation';
import customScalarResolver from "./dates";
const resolvers : IResolvers = {
    ...query,
    ...mutation,
    ...customScalarResolver,
}

export default resolvers;