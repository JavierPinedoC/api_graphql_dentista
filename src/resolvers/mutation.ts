import { IResolvers } from "graphql-tools";
import { Datetime } from "../lib/datetime";
import bcryptjs from "bcryptjs";
const mysql = require("mysql2");
const { Client } = require("pg");
import { cpg, cmys } from "../config/database";

const client = new Client({ connectionString: cpg});
client.connect((err:any)=>{
    if (err) {
        console.error("Error:- " + err.stack);
        return;
      }
      console.log("Connected correct");
});

const connection = mysql.createConnection(cmys);
connection.connect(function (err: any) {
  if (err) {
    console.error("Error:- " + err.stack);
    return;
  }
  console.log("Connected Id:- " + connection.threadId);
});

const mutation: IResolvers = {
  Mutation: {
    // async register(__:void,{user},{db}): Promise<any>{
    //     const userCheck = await db.collection('users').findOne({email:user.email});
    //     if (userCheck!==null) {
    //         return{
    //             status: false,
    //             message: `Ya existe el usuario ${user.email}`,
    //             user: null
    //         };
    //     }
    //     const lastUser = await db.collection('users').find().limit(1).sort({registerDate: -1}).toArray();
    //     if (lastUser.length === 0) {
    //         user.id =1;
    //     }
    //     else{
    //         user.id = lastUser[0].id +1;
    //     }

    //     user.password = bcryptjs.hashSync(user.password,10)
    //     user.registerDate = new Datetime().getCurrentDateTime();
    //     return await db.collection('users').insertOne(user)
    //     .then((result: any)=> {
    //         return{
    //             status: true,
    //             message: `Usuario ${user.name} ${user.lastname} añadido correctamente`,
    //             user
    //         };
    //     })
    //     .catch((err: any)=>{
    //         return{
    //             status: false,
    //             message: `Usuario NO añadido correctamente`,
    //             user: null
    //         };
    //     });

    // },
    async userpg(__: void, { userpg }): Promise<any> {
      const conv = Object.values(userpg);
      // console.log(conv);
      return await client
        .query("INSERT INTO users (id,username,password)VALUES($1,$2,$3)", conv)
        .then((result: any) => {
          return {
            status: true,
            message: `Usuario ${userpg.username} añadido correctamente`,
          };
        });
    },
    async marcanew(__: void, { marca }): Promise<any> {
      const conv = Object.values(marca);
      // console.log(conv);
      return await connection
        .promise()
        .execute("INSERT INTO marcaphone (marcaid, marca)VALUES(?,?)", conv)
        .then((result: any) => {
          return {
            status: true,
            message: `Usuario ${marca.marca} añadido correctamente`,
          };
        });
    },
    async registerUser(__: void, { user }): Promise<any> {
        let em = {email: user.email}        
        const userCheck = await client.query('SELECT email from users WHERE email = $1',Object.values(em));       
        if (userCheck.rowCount>0) {
            return{
                status: false,
                message: `Ya existe el usuario ${user.email}`,
                user: null
            };
        }
      user.password = bcryptjs.hashSync(user.password, 10);
      const conv = Object.values(user);
    //   console.log(conv);
      return await client
        .query("INSERT INTO users (nombre_usuario,apellido_usuario,email,password, telefono,fecha_nacimiento,genero,id_tipo_usuario)VALUES($1,$2,$3,$4,$5,$6,$7,$8)", conv)
        .then((result: any) => {
          return {
            status: true,
            message: `Usuario ${user.nombre_usuario} añadido correctamente`,
            user: user
          };
        });
    },
    async generarhistorialc(__:void,clinic,{db}): Promise<any>{      
      const lastUser = await db.collection('hitorial_Clinico').find().limit(1).sort({ id_historial_clinico: -1 }).toArray();
      if (lastUser.length === 0) {
       clinic.historialc.id_historial_clinico = 1;
      } else {
         clinic.historialc.id_historial_clinico = lastUser[0].id_historial_clinico + 1;
      }
      return await db.collection('hitorial_Clinico').insertOne(clinic.historialc)
            .then((result:any)=>{
              return {
                status: true,
                id_paciente: clinic.historialc.id_historial_clinico,
                histoClini: clinic.historialc
              }
            }).catch((err:any)=>{
              return{
                status: false,
                id_paciente: null,
                histoClini: null
                }
              }
            )
    },
    async setCita(__:void,{cita}): Promise<any>{
      const conv = Object.values(cita);
      const result = await connection.promise().execute("INSERT INTO Citas(idPacientes, Tratamiento_idTratamiento,fechaUsuario_cita)Values(?,?,?)",conv).then(([rows,fields]:any) => {
       return{
         status: true
       }
      });
      return result;
    }
  },
};

export default mutation;
