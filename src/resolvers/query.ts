import { IResolvers } from "graphql-tools";
import JWT from "../lib/jwt";
import bcryptjs from "bcryptjs";
const mysql = require("mysql2");
const { Client } = require("pg");
import { cpg, cmys } from "../config/database";

const client = new Client({ connectionString: cpg });
client.connect();

const connection = mysql.createConnection(cmys);
connection.connect(function (err: any) {
  if (err) {
    console.error("Error:- " + err.stack);
    return;
  }
  console.log("Connected Id:- " + connection.threadId);
});

const query: IResolvers = {
  Query: {
    async users(_: void, __: any, { db }): Promise<any> {
      const a = await db.collection("users").find().toArray();
      return a;
    },
    async gethistclinico(_: void, {id_paciente,token}, { db }): Promise<any> {
      let aprob:any = new JWT().verify(token);
      if (aprob === "La autenticación del token es invalida, inicia sesión") {
        return{
          status: false,
          id_paciente: null,
          histoClini: null
        }
      } 
      id_paciente = [id_paciente];

      const id_pac = await client.query("SELECT * FROM paciente WHERE id_usuario = $1", id_paciente);
      id_paciente = id_pac.rows[0].id_paciente;
     
      
      const histoClini = await db.collection("hitorial_Clinico").findOne({ id_paciente});
      if (histoClini === null) {
        return {
          status: false,
          id_paciente,
          histoClini: null
        };
      } else {
        return { status: true, id_paciente, histoClini };
      }
      
    },
    async login(_: void, { email, password }): Promise<any> {
      let em = { email };
      const user = await client.query(
        "SELECT * from users WHERE email = $1",
        Object.values(em)
      );
      if (user.rowCount <= 0) {
        return {
          status: false,
          message: "Login INCORRECTO. email",
          token: null,
        };
      }
      if (!bcryptjs.compareSync(password, user.rows[0].password)) {
        return {
          status: false,
          message: "Login INCORRECTO. pass",
          token: null,
        };
      }
      delete user.rows[0].password;
      console.log(user.rows);
      return {
        status: true,
        message: "Login Correcto",
        token: new JWT().sing({ user }),
      };
    },
    me(_: void, __: any, { token }) {
      let info: any = new JWT().verify(token);
      // console.log(info.user[0]);

      if (info === "La autenticación del token es invalida, inicia sesión") {
        return {
          status: false,
          message: info,
          user: null,
        };
      }
      return {
        status: true,
        message: "Token Correcto",
        user: info.user[0],
      };
    },
    async userpg(_: void, __: any, { dbpg }): Promise<any> {
      const result = await client.query({ text: "SELECT * FROM users" });
      // other option to convert dates to string -> TO_CHAR(created_at,'DD/MM/YYYY')created_at

      // console.log(result.rows);

      return result.rows;
    },
    async marcamovil(_: void, __: any, { dbmy }): Promise<any> {
      const rows = await connection
        .promise()
        .execute("SELECT * from marcaphone")
        .then(([rows, fields]: any) => {
          // console.log(rows);
          // connection.end();
          return rows;
        });
      return rows;
    },
    async getTratamineto(_:void, __:any, {tratamiento}): Promise<any>{
      const gettrata = await connection.promise().execute("SELECT * FROM Tratamiento").then(([rows,fields]:any)=>{
        return rows;
      });
      return gettrata;
    },
    async getPacienteid(_:void, {id_usuario}): Promise<any>{
      id_usuario = [id_usuario];
      const result = await client.query("Select * from paciente WHERE id_usuario = $1",id_usuario);      
      return result.rows[0];
    },
    async getCitaId(_:void,{idPacientes}): Promise<any>{
      idPacientes = [idPacientes]
      console.log(idPacientes);
      const result = await connection.promise().execute("SELECT Citas.idPacientes, Citas.estatus_cita, Tratamiento.nombre_tratamiento, Tratamiento.costo_tratamiento, Citas.fecha_cita FROM Citas INNER JOIN Tratamiento ON Citas.Tratamiento_idTratamiento=Tratamiento.idTratamiento WHERE idPacientes=? ORDER BY fecha_cita;",idPacientes).then(([rows,fields]:any)=>{
        // console.log(rows);
        return rows;
        
      })
      console.log(result);
      return result;
    },
    async getHistorialDen(_:void,{idpacientes}): Promise<any>{
      const result = await connection.promise().execute("SELECT * FROM CitasHoy").then(([rows,fields]:any)=> {
        return rows;
      });
      console.log(result);
      return result;
    },
  },
};

export default query;
