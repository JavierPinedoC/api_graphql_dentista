import { DateTimeResolver } from "graphql-scalars";
const customScalarResolver = {
  DateTime: DateTimeResolver,
};

export default customScalarResolver;
