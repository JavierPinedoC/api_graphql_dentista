import { GraphQLSchema } from "graphql";
import 'graphql-import-node';
// import typeDefs from './schema.graphql';
import resolvers from './../resolvers/resolversMap';
import { makeExecutableSchema } from "graphql-tools";
import { mergeTypeDefs } from '@graphql-tools/merge';
import { loadFilesSync } from '@graphql-tools/load-files';

const typeDefs = mergeTypeDefs(loadFilesSync(`${__dirname}/**/*.graphql`));

const schema: GraphQLSchema = makeExecutableSchema({
    typeDefs,
    resolvers
});

export default schema;