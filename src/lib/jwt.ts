import { SECRET_KEY } from "../config/constants";
import jwt from "jsonwebtoken";
class JWT {
  private secretKey = SECRET_KEY as string;
  sing(data: any): string {
    return jwt.sign({user: data.user.rows}, this.secretKey, {expiresIn: 24 * 60 * 60});
  }
  verify(token: string): string {
    try {
        return jwt.verify(token,this.secretKey)as string;
    } catch (e) {
        return 'La autenticación del token es invalida, inicia sesión'
    }
  }
}
export default JWT;