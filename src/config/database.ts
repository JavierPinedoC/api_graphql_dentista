import MongoClient from "mongodb";
import chalk from "chalk";
class Database {
  async init() {
    const MONGODB = String(process.env.DATABASE);
    const client = await MongoClient.connect(MONGODB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    const db = await client.db();

    if (client.isConnected()) {
      console.log("======DATABASE========");
      console.log(`STATUS: ${chalk.greenBright("ONLINE")}`);
      console.log(`DATABASE: ${chalk.greenBright(db.databaseName)}`);
    }
    return db;
  }
}

export default Database;

export const cpg = 'postgres://qejryclu:PIvYWa48fIcvkM2tdW2h4tsM-2BMHGTS@hansken.db.elephantsql.com:5432/qejryclu'

//localhols in psql
//   user: "javipc",
//   host: "localhost",
//   database: "dentist",
//   password: "0301",
//   port: 5432,

export const cmys = {
  host: "localhost",
  user: "root",
  password: "0301",
  database: "dentista",
  debug: false,
};
